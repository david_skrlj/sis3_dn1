<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
  <html>
  <head>
    <h1>News section</h1>
    <link rel="stylesheet" type="text/css" href="https://www.studenti.famnit.upr.si/~89181060/codeignitor-news-tutorial/assets/css/style.css" />
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro|Open+Sans+Condensed:300|Raleway' rel='stylesheet' type='text/css' />
  </head>

  <body>
        <a href="https://www.studenti.famnit.upr.si/~89181060/codeignitor-news-tutorial/index.php/home">Home</a><br />
        <a href="https://www.studenti.famnit.upr.si/~89181060/codeignitor-news-tutorial/index.php/about">About</a><br />        
        <a href="https://www.studenti.famnit.upr.si/~89181060/codeignitor-news-tutorial/index.php/news">News</a><br />
        <a href="https://www.studenti.famnit.upr.si/~89181060/codeignitor-news-tutorial/index.php/news/create">Create News</a><br />
        <a href="https://www.studenti.famnit.upr.si/~89181060/codeignitor-news-tutorial/index.php/user_authentication/index">Signin</a><br />
        <a href="https://www.studenti.famnit.upr.si/~89181060/codeignitor-news-tutorial/index.php/user_authentication/signup">Signup</a><br />
        <a href="https://www.studenti.famnit.upr.si/~89181060/codeignitor-news-tutorial/index.php/user_authentication/logout">Logout</a><br />
        <a href="https://www.studenti.famnit.upr.si/~89181060/codeignitor-news-tutorial/index.php/user_authentication/admin">Admin</a><br />
        <a href="https://www.studenti.famnit.upr.si/~89181060/codeignitor-news-tutorial/index.php/news/books">Books store</a><br /><br />
      <xsl:for-each select="news/news_item">
        <h3><xsl:value-of select="text" /></h3>
        <div class="main">
          <xsl:value-of select="text" />
        </div>
        <p><a href="https://www.studenti.famnit.upr.si/~89181060/codeignitor-news-tutorial/index.php/news/{slug}">View article</a></p>
        <td><xsl:value-of select="title" /></td>
        <td><xsl:value-of select="text" /></td>
        <td><xsl:value-of select="slug" /></td>
       </xsl:for-each>
  </body>
  </html>
</xsl:template>
</xsl:stylesheet>