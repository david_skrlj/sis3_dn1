<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
  <html>
  <head>
    <h1>Books</h1>
    <link rel="stylesheet" type="text/css" href="https://www.studenti.famnit.upr.si/~89181060/codeignitor-news-tutorial/assets/css/style.css" />
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro|Open+Sans+Condensed:300|Raleway' rel='stylesheet' type='text/css' />
  </head>

  <body>
    <h1>My Book Collection</h1>
        <a href="https://www.studenti.famnit.upr.si/~89181060/codeignitor-news-tutorial/index.php/home">Home</a><br />
        <a href="https://www.studenti.famnit.upr.si/~89181060/codeignitor-news-tutorial/index.php/about">About</a><br />        
        <a href="https://www.studenti.famnit.upr.si/~89181060/codeignitor-news-tutorial/index.php/news">News</a><br />
        <a href="https://www.studenti.famnit.upr.si/~89181060/codeignitor-news-tutorial/index.php/news/create">Create News</a><br />
        <a href="https://www.studenti.famnit.upr.si/~89181060/codeignitor-news-tutorial/index.php/user_authentication/index">Signin</a><br />
        <a href="https://www.studenti.famnit.upr.si/~89181060/codeignitor-news-tutorial/index.php/user_authentication/signup">Signup</a><br />
        <a href="https://www.studenti.famnit.upr.si/~89181060/codeignitor-news-tutorial/index.php/user_authentication/logout">Logout</a><br />
        <a href="https://www.studenti.famnit.upr.si/~89181060/codeignitor-news-tutorial/index.php/user_authentication/admin">Admin</a><br />
        <a href="https://www.studenti.famnit.upr.si/~89181060/codeignitor-news-tutorial/index.php/news/books">Books store</a><br /><br />
    <table border="1">
      <tr bgcolor="#9acd32">
        <th>Title</th>
        <th>Auhtor</th>
        <th>YEar</th>
        <th>Price</th>
      </tr>
      <xsl:for-each select="bookstore/book">
      <tr>
        <td><xsl:value-of select="title" /></td>
        <td>
          <xsl:for-each select="author"><xsl:value-of select="." /></xsl:for-each>
        </td>
        <td><xsl:value-of select="year" /></td>
        <td><xsl:value-of select="price" /></td>
         </tr>
       </xsl:for-each>
    </table>
  </body>
  </html>
</xsl:template>
</xsl:stylesheet>