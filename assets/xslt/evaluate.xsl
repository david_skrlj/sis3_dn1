<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
  <html>
  <head>
    <h1>News section</h1>
    <link rel="stylesheet" type="text/css" href="https://www.studenti.famnit.upr.si/~89181060/sis3_dn1/assets/css/style.css" />
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro|Open+Sans+Condensed:300|Raleway' rel='stylesheet' type='text/css' />
  </head>

  <body>
        <a href="https://www.studenti.famnit.upr.si/~89181060/sis3_dn1/index.php/dn1/insert_y">Vnos naloga</a><br />
        <a href="https://www.studenti.famnit.upr.si/~89181060/sis3_dn1/index.php/dn1/evaluate_y">Ovrednotenje naloga</a><br />        
        <a href="https://www.studenti.famnit.upr.si/~89181060/sis3_dn1/index.php/usr_authentication_dn1/signin">Login</a><br />
        <a href="https://www.studenti.famnit.upr.si/~89181060/sis3_dn1/index.php/usr_authentication_dn1/signup">Adminstrator</a><br />
        <a href="https://www.studenti.famnit.upr.si/~89181060/sis3_dn1/index.php/dn1/about">O izdelku</a><br />
        <h2>Potrditev nalogov.</h2>
      <xsl:for-each select="news/news_item">
        <h3>Izvajalec: <xsl:value-of select="worker_name" /></h3>
        <h3>Izvajalec: <xsl:value-of select="tak_check" /></h3>
        <div class="main">Opis naloge:
          <xsl:value-of select="task_decription" />
        </div>
        <p><a href="https://www.studenti.famnit.upr.si/~89181060/sis3_dn1/index.php/dn1/confirm/{id}">Potrdi nalog</a></p>
       </xsl:for-each>
  </body>
  </html>
</xsl:template>
</xsl:stylesheet>