<?php 
class Usr_authentication_dn1 extends CI_Controller {
	public function __construct() {
		parent::__construct();

		$this->load->helper('url_helper');

		// Load form helper library
		$this->load->helper('form');

		// Load form validation library
		$this->load->library('form_validation');

		// Load session library
		$this->load->library('session');

		// Load database
		$this->load->model('login_database');
		
	}

	// Show login page
	public function index() {
		$this->load->view('templates/header_dn1');
		$this->load->view('user_authentication_dn1/login_form');
		$this->load->view('templates/footer');
	}

	// Show registration page
	public function show() {
		$this->load->view('templates/header_dn1');
		$this->load->view('user_authentication_dn1/registration_form');
		$this->load->view('templates/footer');
	}

	// Validate and store registration data in database
	public function signup() {

		// Check validation for user input in SignUp form
		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('email_value', 'Email', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		$this->form_validation->set_rules('userlevel', 'Userlevel', 'trim|required|integer');

		//login?
		if (isset($this->session->userdata['logged_in'])) {
			echo 'login ok!!';
			//level?
			if (($_SESSION['logged_in']['user_level'])==3) {
				echo '***level ok';
				if ($this->form_validation->run() == FALSE) {
					$this->load->view('templates/header_dn1');
					$this->load->view('user_authentication_dn1/registration_form');
					$this->load->view('templates/footer');
				} else {
					//filter to user leve 1<=level<=3
					$u_level=(0+$this->input->post('userlevel'));
					if ($u_level < 1) {
						$u_level=1;
					}
					if ($u_level > 3) {
						$u_level=3;
					}

					$data = array(
						'user_name' => $this->input->post('username'),
						'user_email' => $this->input->post('email_value'),
						'user_password' => $this->input->post('password'),
						'user_level' => $u_level
					);
					
					$result = $this->login_database->registration_insert($data);
					
					if ($result == TRUE) {
						$data['message_display'] = 'Registration Successfully !';
						$this->load->view('templates/header_dn1');
						$this->load->view('user_authentication_dn1/login_form', $data);
						$this->load->view('templates/footer');
					} else {
						$this->load->view('templates/header_dn1');
						$this->load->view('user_authentication_dn1/registration_form');
						$this->load->view('templates/footer');
					}
				}
			} else {
				//not permited
				$data['message_display'] = 'Nimate pravic za urejanje !';
				$this->load->view('templates/header_dn1',$data);
				//$this->load->view('user_authentication_dn1/login_form', $data);
				$this->load->view('dn1/wrong_level');
				$this->load->view('templates/footer');
			}
			
		} else {
			//not logged
			$data['message_display'] = 'Za urejane je potrebno admnistratorka prijava !';
			$this->load->view('templates/header_dn1');
			$_SESSION['call_login']='signup';
			$this->load->view('user_authentication_dn1/login_form', $data);
			$this->load->view('templates/footer');

		}
		
		
	}

	public function admin(){

			if(isset($this->session->userdata['logged_in'])){
				$data['username'] = $this->session->userdata['logged_in']['username'];
				$data['email'] = $this->session->userdata['logged_in']['email'];

				$this->load->view('templates/header_dn1');
				$this->load->view('user_authentication_dn1/admin_page', $data);
				$this->load->view('templates/footer');
			}else{
				$data['message_display'] = 'Signin to view admin page!';
				$this->load->view('templates/header_dn1');
				$this->load->view('user_authentication_dn1/login_form', $data);
				$this->load->view('templates/footer');
			}
	}

	// Check for user login process
	public function signin() {

		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');

		$data = array(
			'username' => $this->input->post('username'),
			'password' => $this->input->post('password')
		);
		$result = $this->login_database->login($data);
		//login correct

		if ($result == TRUE) {

			$username = $this->input->post('username');
			$result = $this->login_database->read_user_information($username);
			if ($result != false) {

				$session_data = array(
					'username' => $result[0]->user_name,
					'email' => $result[0]->user_email,
					'user_level' => $result[0]->user_level,
				);
				// Add user data in session
				$data = array('error_message' => 'Uspešna prijava');
				$this->session->set_userdata('logged_in', $session_data);
				$this->load->view('templates/header_dn1', $data);
				
				if (isset($_SESSION['logged_in'])) {
					$usr_level=$_SESSION['logged_in']['user_level'];
				} else {
					$usr_level=0;
				}
				if (isset($_SESSION['call_login'])) {
					$call_login=$_SESSION['call_login'];
				} else {
					$call_login='';
				}
				
				switch ($usr_level) {
					case 0:
						$this->load->view('user_authentication_dn1/login_form',$data);
						break;
					case 1:
						$this->load->view('dn1/create');
						break;
					case 2:
					//todo
						$this->load->view('dn1/create');
						break;
					case 3:
						$this->load->view('user_authentication_dn1/registration_form');
						break;					
					default:
						$this->load->view('dn1/wrong_level');
						break;
				}
				/*
				//login form
				$this->load->view('user_authentication_dn1/login_form',$data);
				
				if ($usr_level == 0) {
					$this->session->set_userdata('logged_in', NULL);
					$this->load->view('dn1/wrong_level');
				}
				if (($usr_level == 1)&&($call_login == 'insert_y')) {
					$this->load->view('dn1/create');
				}
				if (($usr_level == 2)&&($call_login == 'insert_y')) {
					$this->load->view('dn1/create');
				}
				if (($usr_level == 2)&&($call_login == 'evaluate_y')) {
					//todo!!!
					$this->load->view('dn1/create');
				}
				if ($usr_level==3) {
					$this->load->view('user_authentication_dn1/registration_form');
				}
				
				$this->load->view('templates/footer');
				*/
			}
		} else {	//login incorect
			$data = array(
				'error_message' => 'Nepravilno geslo ali uporabnišo ime'
			);
			$this->load->view('templates/header_dn1');
			$this->load->view('user_authentication_dn1/login_form', $data);
			$this->load->view('templates/footer');
		}
	}

	// Logout from admin page
	public function logout() {

		// Removing session data
		$sess_array = array(
			'username' => ''
		);
		$this->session->unset_userdata('logged_in', $sess_array);
		$data['message_display'] = 'Successfully Logout';
		$this->load->view('templates/header_dn1');
		$this->load->view('user_authentication_dn1/login_form', $data);
		$this->load->view('templates/footer');
	}

	
}

