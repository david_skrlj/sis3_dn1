<?php 
/**
 * 
 */
class Dn1 extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		//$this->load->model('news_model');
		$this->load->helper('url_helper');

		// Load form helper library
		$this->load->helper('form');

		// Load form validation library
		$this->load->library('form_validation');

		// Load session library
		$this->load->library('session');

		//Load database
		$this->load->model('nalog_model');

	}

	function index()
	{
		$data['title'] = 'Dn 1';

	    $this->load->view('templates/header_dn1', $data);
	    $this->load->view('dn1/welcome');
	    $this->load->view('templates/footer', $data);
	}

	function insert_y()
	{
		//$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('text', 'Text', 'required');

		//is login correct
		if(isset($this->session->userdata['logged_in'])){
				$data['title'] = 'Vnos naloga';
				if($this->form_validation->run() === FALSE){
				$this->load->view('templates/header_dn1', $data);
       			$this->load->view('dn1/create');
        		$this->load->view('templates/footer');
				}else{
					$this->dn1_model->set_news();
					$this->load->view('dn1/success');
				}
		}else{
			//not looged
			$data['message_display'] = 'Za unos se je potrebno vpisat v sistem.';
			$this->load->view('templates/header_dn1',$data);
			$this->session->set_userdata('call_login', 'insert_y');
			$this->load->view('user_authentication_dn1/login_form',$data);
			$this->load->view('templates/footer');
		}
	}

	function evaluate_y()
	{
	    
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('text', 'Text', 'required');

		//is login correct to do eaaluate 
		if(isset($this->session->userdata['logged_in'])){
				$data['title'] = 'Ovrednotenje naloga';
				if($this->form_validation->run() === FALSE){
					$data['nalog'] = $this->nalog_model->get_news();

					//evaluate view page to replace with xml
					/*	
					$this->load->view('templates/header_dn1', $data);						
	       			$this->load->view('dn1/entry',$data);
	        		$this->load->view('templates/footer');
					*/
	        		//new code
	        		header('Content-type: text/xml');
        			$xml = new SimpleXMLElement('<news/>');

        			$startXML = '<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="'.base_url().'assets/xslt/evaluate.xsl"?><news></news>';

					$xml = simplexml_load_string($startXML);
										
					foreach ($data['nalog'] as $news_item){
					    $new_itme_node = $xml->addChild("news_item");
					    //var_dump($news_item);
					    $fliped = array_flip($news_item);
					    array_walk_recursive($fliped, array ($new_itme_node, 'addChild'));    
					}
					print $xml->asXML();



				}else{
					$this->nalog_model->set_news();
					$this->load->view('dn1/success');
				}
		}else{
			//not looged
			$data['message_display'] = 'Za unos se je potrebno vpisat v sistem.';
			$this->load->view('templates/header_dn1',$data);
			$this->session->set_userdata('call_login', 'evaluate_y');
			$this->load->view('user_authentication_dn1/login_form',$data);
			$this->load->view('templates/footer');
		}	
	}

	function confirm($id)
	{
		$data['title'] = '**';
		$this->load->view('templates/header_dn1', $data);
		if ($this->nalog_model->confirm_nalog_where($id)) {
			$data['input'] = 'Pordiev uspela.';
			$this->load->view('dn1/print_input',$data);
		} else {
			$data['input'] = 'Pordiev spodletela.';
			$this->load->view('dn1/input',$data);
		}
		
		//$this->load->view('user_authentication_dn1/login_form');
		$this->load->view('templates/footer');
	}

	function about()
	{
		$data['title'] = 'O nalogi';

	    $this->load->view('templates/header_dn1', $data);
	    $this->load->view('dn1/about');
	    $this->load->view('templates/footer', $data);	
	}

	// Logout from admin page
	public function logout() {

		// Removing session data
		$sess_array = array(
			'username' => ''
		);
		$this->session->unset_userdata('logged_in', $sess_array);
		$data['message_display'] = 'Niste prijavljeni v sistem!';
		$this->load->view('templates/header_dn1', $data);
		$this->load->view('user_authentication_dn1/login_form');
		$this->load->view('templates/footer');
	}

	public function print_json(){
		$data['title'] = 'Json output';
		$this->load->view('templates/header_dn1',$data);
		$my_json=json_encode($this->nalog_model->get_news());
		$data['json_content']=$my_json;
		$this->load->view('dn1/show_json',$data);
		$this->load->view('templates/footer');
	}

}






