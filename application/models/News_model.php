<?php
class News_model extends CI_Model{

	public function __construct(){
		$this->load->database();
	}

	public function get_news(){
		$query = $this->db->get('working_task');
		return $query->result_array();
	}

	public function get_news_where($slug){
		$query = $this->db->get_where('news', array('slug' => $slug));
		return $query->row_array();
	}

	public function set_news(){
		$slug = url_title($this->input->post('title'), 'dash', TRUE);
		$data = array(
			'title' => $this->input->post('title'),
			'slug' => $slug,
			'text' => $this->input->post('text'));
		return $this->db->insert('news', $data);			
	}

	
}