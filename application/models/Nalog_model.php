<?php
/*
	copy of News_model
*/
class Nalog_model extends CI_Model{

	public function __construct(){
		$this->load->database();
	}

	public function get_news(){
		$query = $this->db->get('working_task');
		return $query->result_array();
	}

	public function get_news_where($slug){
		$query = $this->db->get_where('working_task', array('slug' => $slug));
		return $query->row_array();
	}

	public function set_news(){
		$worker=("Create by: ".$_SESSION['logged_in']['username']." At: ".date('d/m/Y-h:i'));
		//$worker=$_SESSION['logged_in']['username'];
		$data = array(
			'worker_name' => $worker,
			'task_decription' => $this->input->post('text')
		);
		return $this->db->insert('working_task', $data);			
	}

	public function confirm_nalog_where($id){
		$worker=("Potrdil ".$_SESSION['logged_in']['username']." at: ".date('d/m/Y-h:i'));
		$data = array(
			'tak_check' => $worker
		);
		
		$this->db->set($data );
		$this->db->where('id',$id);
		return $this->db->update('working_task');

			/*return $this->db->insert('working_task', $data);
			$data = array(
		        'title' => $this->input->post('title'),
		        'slug' => $slug,
		        'text' => $this->input->post('text')
		    );*/
	}
}