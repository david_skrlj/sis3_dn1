<?php
if (isset($logout_message)) {
	echo "<div class='message'>";
	echo $logout_message;
	echo "</div>";
}
?>
<?php
if (isset($message_display)) {
	echo "<div class='message'>";
	echo $message_display;
	echo "</div>";
}
?>
<div id="main">
	<div id="login">
		<h2>Prijava uporabnika.</h2>
		<hr/>
		<?php echo form_open('usr_authentication_dn1/signin'); ?>
		<?php
		echo "<div class='error_msg'>";
		if (isset($error_message)) {
			echo $error_message;
		}
		echo validation_errors();
		echo "</div>";
		?>
		<label>Uporabnik :</label>
		<input type="text" name="username" id="name" placeholder="uporabnik"/><br /><br />
		<label>Geslo :</label>
		<input type="password" name="password" id="password" placeholder="**********"/><br/><br />
		<input type="submit" value=" Login " name="submit"/><br />
		<a href="<?php echo base_url() ?>index.php/usr_authentication_dn1/show">Za vpis klikni tu</a>
		<?php echo form_close(); ?>
	</div>
</div>
